# Godox menu - Go docs with dmenu

Just a small tool to find public functions, types, variables i Golang docs.

Platform: Linux

Requirement: [dmenu](https://tools.suckless.org/dmenu), Go, make

Usage: ./godox-menu
